import { when } from 'ts-mockito'
import { A } from '.'
import { t } from './util'

test('accessing by idx', () => {
  const { sut, dep } = t(A)

  const r1 = sut.afn()
  expect(r1).toEqual(0)

  when(dep(0).bfn()).thenReturn(2)
  when(dep(1).cfn()).thenReturn(3)

  const r2 = sut.afn()
  expect(r2).toEqual(6)
})

// test('accessing by ctor', () => {
//   const { sut, dep } = t(A)
//
//   const r1 = sut.afn()
//   expect(r1).toEqual(0)
//
//   when(dep(B).bfn()).thenReturn(2)
//   when(dep(C).cfn()).thenReturn(3)
//
//   const r2 = sut.afn()
//   expect(r2).toEqual(6)
// })
