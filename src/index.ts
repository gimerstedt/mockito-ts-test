class B {
  bfn = () => 4
}

class C {
  cfn = () => 3
}

export class A {
  static inject = [B, C]

  constructor(public b: B, public c: C) {}

  afn() {
    return this.b.bfn() * this.c.cfn()
  }
}
