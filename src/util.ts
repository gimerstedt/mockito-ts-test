import { mock, instance } from 'ts-mockito'

type Ctor = { new (...x: any): any }

// type static inject?
// type Injectable<T extends Ctor> = { inject: T[] }

// extract the return type somehow..
// type tret<
//   C extends Ctor,
//   I extends number,
// > = {
//   sut: InstanceType<C>
//   get: (idx: I) => ConstructorParameters<C>[I]
// }

type DepInstance<T extends Ctor> = InstanceType<ConstructorParameters<T>>

type Mocks<C extends Ctor> = {
  ctor: C
  mock: DepInstance<C>
  instance: DepInstance<C>
}[]

export function t<C extends Ctor>(ctor: C) {
  const mocks: Mocks<C> = (ctor as any).inject.map((dep: any) => {
    const m = mock(dep)
    const i = instance(m)
    return {
      ctor: dep, // ?? used access in get() ??
      mock: m,
      instance: i,
    }
  })

  const sut: InstanceType<C> = new ctor(...mocks.map(m => m.instance))

  const dep = <_ extends ConstructorParameters<C>[Idx], Idx extends number>(
    idx: Idx,
  ): ConstructorParameters<C>[Idx] => mocks[idx].mock

  return {
    sut,
    dep,
  }
}
